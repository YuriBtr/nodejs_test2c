import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import _ from 'lodash';

const __DEV__ = true;

const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/task2C', (req, res, next) => {
  let username = req.query.username;
  console.log('Entered: ' + username);

  if (!username) {
    console.log('Invalid username');
    res.send('Invalid username');
    return;
  }

  const regexp = new RegExp('[^\\/](\\/[@._\\w]+\\/)', 'g');
  const tmp = username.match(regexp);
  if (tmp) {
    username = tmp[0].substring(2, tmp[0].length - 1);
  } else {
    const posStart = username.lastIndexOf('/') + 1;
    let posEnd = username.lastIndexOf('?');
    if (posEnd <= 0) posEnd = username.length;
    username = username.substring(posStart, posEnd);
  }

  if (username[0] !== '@') username = '@' + username;
  console.log ('Adopted: ' + username);
  res.send(username);
  return;
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
